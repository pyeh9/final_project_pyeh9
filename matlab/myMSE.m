function out = myMSE(pic, template, centerr, centerc)
[maxM maxN x] = size(pic);
[M N x] = size(template);

MSE = 0;
temp1 = centerr-floor(M/2);
temp2 = centerr+floor(M/2);
temp3 = centerc-floor(N/2);
temp4 = centerc+floor(N/2);

if(temp1<1 || temp2>maxM || temp3<1 || temp4>maxN)
    MSE = 9999;
%     for r = 1:M
%         for c = 1:N
%             imager = centerr-floor(M/2)+(r-1);
%             imagec = centerc-floor(N/2)+(c-1);
%             if(imager>1 && imagec>1 && imager<=maxM && imagec<=maxN)
%                 MSE = MSE + sum((template(r,c,:)-pic(imager, imagec,:)).^2);
%             end
%         end
%     end
else
    MSE = (pic(temp1:temp2, temp3:temp4,:) - template).^2;
    MSE = sum(MSE(:));
end

out = MSE/(M*N);

end