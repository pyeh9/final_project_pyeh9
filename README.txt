Particle Filter 
Peihsun (Ben) Yeh
CSE6230
with thanks to Kevin Schluff

Dependencies:
- C++11
- OpenCV 2.4 
  - with Qt build
  - optional: with GPU build if you want to run GPU version)

This project contains:
- matlab: initial code written in matlab for proof of concept and understanding of the algorithm
- docs: various documents associated with the assignment. Includes
	- Proposal
	- Literature review
- cpp_v1: c++ version of code.
- cpp: an experimental version of the code

To build and run:
cd cpp_v1/
make particle_filter 
pf [-p N] video_file.avi
OR 
make particle_filter_gpu
pf_gpu [-p N] video_file.avi

Ex:
pf ../test_inputs/seqI/Vid_I_person_crossing.avi

Then select a region to track, and type 'y' in standard input
