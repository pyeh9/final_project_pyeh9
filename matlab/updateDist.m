function out = updateDist(dist, rin, cin, p)
M = 7;
[maxM maxN] = size(dist);
border = floor(M/2);
temp = zeros(maxM+2*border, maxN+2*border);
%temp = ones(maxM+2*border, maxN+2*border)/((maxM+2*border)*(maxN+2*border));

rin(rin<1) = 1;
rin(rin>maxM) = maxM;
cin(cin<1) = 1;
cin(cin>maxN) = maxN;

%temp(border+1:maxM+border, border+1: maxN+border) = dist; % edit

for i = 1:length(rin)
    add = p(i)*fspecial('gaussian', M, 3);
    r = rin(i)+border;
    c = cin(i)+border;
    temp(r-border:r+border, c-border:c+border) = temp(r-border:r+border, c-border:c+border)+add; 
end

temp = temp(border+1:maxM+border, border+1:maxN+border);
out = temp/sum(temp(:));
end