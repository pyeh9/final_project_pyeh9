#ifndef FILTER_H
#define FILTER_H

#include <opencv2/opencv.hpp>
#include <vector>
#include <opencv2/gpu/gpu.hpp>
enum FilterStates {ST_X, ST_Y, ST_XDOT, ST_YDOT, ST_SCALE, NUM_STATES};

class ParticleFilter {
public:
  ParticleFilter(unsigned int n_particles);
  ~ParticleFilter();
  
  void init(const cv::Rect& selection);
  void init_sample(const float initial[], const float std[]);
  void draw_particles(cv::Mat& image, const cv::Size& target_size, const cv::Scalar& color);
  float calc_likelihood(cv::Mat& image_roi, cv::Mat& target_bhist, cv::Mat& target_ghist, cv::Mat& target_rhist);
  void redistribute(const float lower_bound[], const float upper_bound[]);
  cv::Mat& update(cv::Mat& image, const cv::Size& target_size, cv::Mat& target_bhist, cv::Mat& target_ghist, cv::Mat& target_rhist);
  void draw_estimated_state(cv::Mat& image, const cv::Size& target_size, const cv::Scalar& color);
  
  void calc_confidence(cv::Mat& image, const cv::Size& target_size, cv::Mat& target_bhist, cv::Mat& target_ghist, cv::Mat& target_rhist);
  
  const cv::Mat& get_state() const { return state; }
  float get_confidence() const { return mean_confidence; }
  void time_update();

  cv::Mat& update_gpu(cv::Mat& image, const cv::Size& target_size, cv::Mat& target_bhist, cv::Mat& target_ghist, cv::Mat& target_rhist);
  //void calc_confidence_gpu(cv::Mat& image, const cv::Size& target_size, cv::Mat& target_bhist, cv::Mat& target_ghist, cv::Mat& target_rhist);

private:
  unsigned int num_states;
  cv::Mat_<float> transition_matrix;
  cv::Mat_<float> state; // vector of current State
  unsigned int num_particles; // number of samples
  std::vector<cv::Mat_<float> > particles; // current particles
  std::vector<float> confidence; // confidence for each particle
  std::vector<cv::Mat_<float> > new_particles;
  std::vector<float> cumulative; // cumulative confidence vector
  cv::Mat_<float> temp; // temporary vector of size num_states x 1 
                        // for various calculations
  cv::RNG rng;
  const float* std_dev;

  float mean_confidence;
};

#endif
