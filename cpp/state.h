#ifndef STATE_H
#define STATE_H

#include <opencv2/opencv.hpp>
#include "selector.h"
#include "filter.h"
using namespace cv;

extern const char *WINDOW;
enum STATE { START, SELECTING, INITIALIZING, TRACKING };

struct StateData {
  cv::Mat image;
  cv::Mat target;
  cv::Mat target_histogram[3];
  Selector selector;
  Rect selection;
  bool paused;
  bool draw_particles;
  ParticleFilter filter;
  StateData (int num_particles):
    image(),
    target(),
    target_histogram(),
    //target_histogram[0](),
    //target_histogram[1](),
    //target_histogram[2](),
    selector(WINDOW),
    selection(),
    paused(false),
    draw_particles(false),
    filter(num_particles)
  {};
};

struct State_;
typedef State_ (*State)(StateData&); // State is a pointer to function that takes in StateData& and returns State_

struct State_ {
  State s;
  State_ (State ss) : s(ss) {}
  operator State() { return s; }
};

/*
State_ state_start (StateData& d);
State_ state_selecting (StateData& d);
State_ state_initializing (StateData& d);
State_ state_tracking (StateData& d);
*/
#endif
