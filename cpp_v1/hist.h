#ifndef HIST_H
#define HIST_H

#include <opencv2/opencv.hpp>

void calc_hist(cv::Mat& bgr, cv::Mat& bhist, cv::Mat& ghist, cv::Mat& rhist); 


#endif
