#include "filter.h"
#include "hist.h"
#include <iostream>
#include <ctime>

#ifdef USE_GPU
#include <cuda_runtime_api.h>
#include <cuda.h>
#include <opencv2/gpu/gpu.hpp>
#include "filter_cuda.h"
#endif

using namespace cv;
using namespace std;

ParticleFilter::ParticleFilter(unsigned int n_particles) :
  num_states(NUM_STATES),
  transition_matrix(num_states, num_states),
  state(num_states, 1),
  num_particles(n_particles),
  particles(),
  confidence(),
  new_particles(),
  cumulative(),
  temp(num_states, 1),
  rng(time(NULL)),
  std_dev(0),
  mean_confidence(0.0f)
{
  const float prob = 1.0/num_particles;
  for (unsigned int i = 0; i < num_particles; i++) {
    particles.push_back(Mat_<float>(num_states,1));
    new_particles.push_back(Mat_<float>(num_states, 1));
    confidence.push_back(prob);
    cumulative.push_back(1.0);
  }
  std::cout << "Particle filter created with " << num_particles << " particles\n";
}

ParticleFilter::~ParticleFilter() {}

void ParticleFilter::time_update() {
  float sum = 0;
  // calculate weighted mean of particles
  temp = Mat_<float>::zeros(temp.size());
  for (unsigned int i = 0; i < num_particles; i++) {
    //state = particles[i] * confidence[i];
    //temp += state;
    temp += (particles[i] * confidence[i]);
    sum += confidence[i];
    cumulative[i] = sum;
  }
  temp *= 1.0f/sum;

  // transform the mean state through dynamics
  state = transition_matrix * temp;
  float mean_confidence = sum/num_particles;
  // resampling. A particle is selected until its confidence is less
  // than the expected cumulative confidence for that index. Not parallelizable for now
  for (unsigned int i = 0; i < num_particles; i++) {
    unsigned int j = 0;
    while((cumulative[j] <= (float)i*mean_confidence) && (j < num_particles-1)) {
      j++;
    }
    particles[j].copyTo(new_particles[i]);
  }
  // Since particle 0 always gets chosen by the above, assign the mean state to it
  state.copyTo(new_particles[0]);
  // randomly perturb the new particles. 
  for (unsigned int i = 0; i < num_particles; i++) {
    for (unsigned int j = 0; j < num_states; j++) {
      temp(j) = rng.gaussian(std_dev[j]);
    }
    particles[i] = transition_matrix * new_particles[i];
    particles[i] += temp;
  }
}

float ParticleFilter::calc_likelihood(Mat& image_roi, Mat& target_bhist, Mat& target_ghist, Mat& target_rhist) {
  static const float LAMBDA = 20.0f;
  static Mat ghist, bhist, rhist;
  calc_hist(image_roi, ghist, bhist, rhist);
  
  float b = compareHist(target_bhist, bhist, CV_COMP_BHATTACHARYYA);
  float g = compareHist(target_ghist, ghist, CV_COMP_BHATTACHARYYA);
  float r = compareHist(target_rhist, rhist, CV_COMP_BHATTACHARYYA);
  float similar = 0.2126*r + 0.7152*g + 0.0722*b;
  // Y = 0.2126R+0.7152G+0.0722B
  float prob = exp(-LAMBDA*(similar*similar));
  return prob;
}

void ParticleFilter::calc_confidence(Mat& image, const Size& target_size, Mat& target_bhist, Mat& target_ghist, Mat& target_rhist) {
  Rect bounds(0, 0, image.cols, image.rows);
  for (unsigned int i = 0; i < num_particles; i++) {
    float scale = MAX(0.1, particles[i](ST_SCALE));
    particles[i](ST_SCALE) = scale;
    int w = round(target_size.width * scale);
    int h = round(target_size.height * scale);
    int x = round(particles[i](ST_X)) - w/2;
    int y = round(particles[i](ST_Y)) - h/2;
    Rect region = Rect(x, y, w, h) & bounds;
    Mat image_roi(image, region);
    //confidence[i] = calc_likelihood(image_roi, target_bhist, target_ghist, target_rhist);
    
    static const float LAMBDA = 20.0f;
    static Mat ghist, bhist, rhist;
    calc_hist(image_roi, ghist, bhist, rhist);
  
    float b = compareHist(target_bhist, bhist, CV_COMP_BHATTACHARYYA);
    float g = compareHist(target_ghist, ghist, CV_COMP_BHATTACHARYYA);
    float r = compareHist(target_rhist, rhist, CV_COMP_BHATTACHARYYA);
    float similar = 0.2126*r + 0.7152*g + 0.0722*b;
    // Y = 0.2126R+0.7152G+0.0722B
    float prob = exp(-LAMBDA*(similar*similar));
    confidence[i] = prob;
  }
}

/*****************************************************************
 * update function - core of particle filter algorithm
 * does prediction, resampling, and mean calculation
 *****************************************************************/
Mat& ParticleFilter::update(Mat& image, const Size& target_size, Mat& target_bhist, Mat& target_ghist, Mat& target_rhist) {  
  // update confidence for each particle
  calc_confidence(image, target_size, target_bhist, target_ghist, target_rhist);
  time_update();

  // update the confidence at the mean state
  Rect bounds(0, 0, image.cols, image.rows);
  float scale = MAX(0.1, state(ST_SCALE));
  state(ST_SCALE) = scale;
  int w = round(target_size.width * scale);
  int h = round(target_size.height * scale);
  int x = round(state(ST_X)) - w/2;
  int y = round(state(ST_Y)) - h/2;
  Rect region = Rect(x, y, w, h) & bounds;
  Mat image_roi(image, region);
  mean_confidence = calc_likelihood(image_roi, target_bhist, target_ghist, target_rhist);
  // Redistribute the particles to reacquire the target if the mean 
  // state moves off screen, or if the target has been lost for any 
  // reason
  if (!bounds.contains(Point(round(state(ST_X)), round(state(ST_Y)))))
  {
    static const float lower_bound[NUM_STATES] = {0, 0, -0.5, -0.5, 1.0};
    static const float upper_bound[NUM_STATES] = {(float)image.cols, (float)image.rows, 0.5, 0.5, 2.0};
    cout << "Redistribute; " << state << " " << mean_confidence << endl;
    redistribute(lower_bound, upper_bound);
  }
  return state;
}

cv::Mat& ParticleFilter::update_gpu(cv::Mat& image, const cv::Size& target_size, cv::Mat& target_bhist, cv::Mat& target_ghist, cv::Mat& target_rhist) {
  Rect bounds(0, 0, image.cols, image.rows);
  for (unsigned int i = 0; i < num_particles; i++) {
    float scale = MAX(0.1, particles[i](ST_SCALE));
    particles[i](ST_SCALE) = scale;
    int w = round(target_size.width * scale);
    int h = round(target_size.height * scale);
    int x = round(particles[i](ST_X)) - w/2;
    int y = round(particles[i](ST_Y)) - h/2;
    Rect region = Rect(x, y, w, h) & bounds;
    Mat image_roi(image, region);

    static const float LAMBDA = 20.0f;
    Mat roi_channel[3];
    Mat roi_hist[3];
    split(image_roi, roi_channel);
    gpu::GpuMat image_roi_gpu[3];
    gpu::GpuMat roi_hist_gpu[3];
    image_roi_gpu[0].upload(roi_channel[0]);
    image_roi_gpu[1].upload(roi_channel[1]);
    image_roi_gpu[2].upload(roi_channel[2]);
    gpu::calcHist(image_roi_gpu[0], roi_hist_gpu[0]);
    gpu::calcHist(image_roi_gpu[1], roi_hist_gpu[1]);
    gpu::calcHist(image_roi_gpu[2], roi_hist_gpu[2]);
    //gpu::normalize(roi_hist_gpu[0], roi_hist_gpu[0]);
    //gpu::normalize(roi_hist_gpu[1], roi_hist_gpu[1]);
    //gpu::normalize(roi_hist_gpu[2], roi_hist_gpu[2]);
    roi_hist_gpu[0].download(roi_hist[0]);
    roi_hist_gpu[1].download(roi_hist[1]);
    roi_hist_gpu[2].download(roi_hist[2]);
    image_roi_gpu[0].release();
    image_roi_gpu[1].release();
    image_roi_gpu[2].release();
    roi_hist_gpu[0].release();
    roi_hist_gpu[1].release();
    roi_hist_gpu[2].release();
    cv::normalize(roi_hist[0], roi_hist[0], 1, 0, NORM_L2, CV_32F);
    cv::normalize(roi_hist[1], roi_hist[1], 1, 0, NORM_L2, CV_32F);
    cv::normalize(roi_hist[2], roi_hist[2], 1, 0, NORM_L2, CV_32F);
    //cout << roi_hist[0] << endl;
    //cout << roi_hist[1] << endl;
    //cout << roi_hist[2] << endl;
    float b = compareHist(target_bhist, roi_hist[0], CV_COMP_BHATTACHARYYA);
    float g = compareHist(target_ghist, roi_hist[1], CV_COMP_BHATTACHARYYA);
    float r = compareHist(target_rhist, roi_hist[2], CV_COMP_BHATTACHARYYA);
    float similar = 0.2126*r + 0.7152*g + 0.0722*b;
    // Y = 0.2126R+0.7152G+0.0722B
    float prob = exp(-LAMBDA*(similar*similar));
    confidence[i] = prob;
  }
  time_update();

  // update the confidence at the mean state
  float scale = MAX(0.1, state(ST_SCALE));
  state(ST_SCALE) = scale;
  int w = round(target_size.width * scale);
  int h = round(target_size.height * scale);
  int x = round(state(ST_X)) - w/2;
  int y = round(state(ST_Y)) - h/2;
  Rect region = Rect(x, y, w, h) & bounds;
  Mat image_roi(image, region);

#ifdef USE_GPU
  static const float LAMBDA = 20.0f;
  Mat roi_channel[3];
  Mat roi_hist[3];
  split(image_roi, roi_channel);
  gpu::GpuMat image_roi_gpu[3];
  gpu::GpuMat roi_hist_gpu[3];
  image_roi_gpu[0].upload(roi_channel[0]);
  image_roi_gpu[1].upload(roi_channel[1]);
  image_roi_gpu[2].upload(roi_channel[2]);
  gpu::calcHist(image_roi_gpu[0], roi_hist_gpu[0]);
  gpu::calcHist(image_roi_gpu[1], roi_hist_gpu[1]);
  gpu::calcHist(image_roi_gpu[2], roi_hist_gpu[2]);
  //gpu::normalize(roi_hist_gpu[0], roi_hist_gpu[0]);
  //gpu::normalize(roi_hist_gpu[1], roi_hist_gpu[1]);
  //gpu::normalize(roi_hist_gpu[2], roi_hist_gpu[2]);
  roi_hist_gpu[0].download(roi_hist[0]);
  roi_hist_gpu[1].download(roi_hist[1]);
  roi_hist_gpu[2].download(roi_hist[2]);
  image_roi_gpu[0].release();
  image_roi_gpu[1].release();
  image_roi_gpu[2].release();
  roi_hist_gpu[0].release();
  roi_hist_gpu[1].release();
  roi_hist_gpu[2].release();
  cv::normalize(roi_hist[0], roi_hist[0], 1, 0, NORM_L2, CV_32F);
  cv::normalize(roi_hist[1], roi_hist[1], 1, 0, NORM_L2, CV_32F);
  cv::normalize(roi_hist[2], roi_hist[2], 1, 0, NORM_L2, CV_32F);
  //cout << roi_hist[0] << endl;
  //cout << roi_hist[1] << endl;
  //cout << roi_hist[2] << endl;
  float b = compareHist(target_bhist, roi_hist[0], CV_COMP_BHATTACHARYYA);
  float g = compareHist(target_ghist, roi_hist[1], CV_COMP_BHATTACHARYYA);
  float r = compareHist(target_rhist, roi_hist[2], CV_COMP_BHATTACHARYYA);
  float similar = 0.2126*r + 0.7152*g + 0.0722*b;
  // Y = 0.2126R+0.7152G+0.0722B
  float prob = exp(-LAMBDA*(similar*similar));
  mean_confidence = prob;
#else
  mean_confidence = calc_likelihood(image_roi, target_bhist, target_ghist, target_rhist);
#endif
  
  // Redistribute the particles to reacquire the target if the mean 
  // state moves off screen, or if the target has been lost for any 
  // reason
  if (!bounds.contains(Point(round(state(ST_X)), round(state(ST_Y)))))
  {
    static const float lower_bound[NUM_STATES] = {0, 0, -0.5, -0.5, 1.0};
    static const float upper_bound[NUM_STATES] = {(float)image.cols, (float)image.rows, 0.5, 0.5, 2.0};
    cout << "Redistribute; " << state << " " << mean_confidence << endl;
    redistribute(lower_bound, upper_bound);
  }
  return state;
}


void ParticleFilter::draw_estimated_state(Mat& image, const Size& target_size, const Scalar& color) {
  Rect bounds(0, 0, image.cols, image.rows);
  for (unsigned int i = 0; i < num_particles; i++) {
    int w = round(target_size.width * state(ST_SCALE));
    int h = round(target_size.height * state(ST_SCALE));
    int x = round(state(ST_X)) - w/2;
    int y = round(state(ST_Y)) - h/2;
    Rect rect = Rect(x, y, w, h) & bounds;
    rectangle(image, rect, color, 2);
  }
}

void ParticleFilter::init_sample(const float initial[], const float std[]) {
  std_dev = std;
  for (unsigned int i = 0; i < num_particles; i++) {
    for (unsigned int j = 0; j < num_states; j++) {
      float r = rng.gaussian(std_dev[j]);
      particles[i](j) = initial[j] + r;
    }
    confidence[i] = 1.0/(float)num_particles;
  }
  for (unsigned int j = 0; j < num_states; j++) {
    state(j) = initial[j];
  }
}

void ParticleFilter::init(const Rect& selection) {
  transition_matrix = *(Mat_<float>(NUM_STATES, NUM_STATES) <<
    1, 0, 1, 0, 0,  // constant velocity model
    0, 1, 0, 1, 0,  // constant scale
    0, 0, 1, 0, 0,  // xnext = x + xdot
    0, 0, 0, 1, 0,  // ynext = y + ydot
    0, 0, 0, 0, 1); //
  const float initial[NUM_STATES] = {
    (float)(selection.x + selection.width/2),
    (float)(selection.y + selection.height/2),
    0, 0, 1.0};
  
  // experiment with these
  static const float std_dev[NUM_STATES] = {2, 2, 0.5, 0.5, 0.1};
  
  init_sample(initial, std_dev);
}

void ParticleFilter::draw_particles(Mat& image, const Size& target_size, const Scalar& color) {
  Rect bounds(0, 0, image.cols, image.rows);
  for (unsigned int i = 0; i < num_particles; i++) {
    int w = round(target_size.width * particles[i](ST_SCALE));
    int h = round(target_size.height * particles[i](ST_SCALE));
    int x = round(particles[i](ST_X)) - w/2;
    int y = round(particles[i](ST_Y)) - h/2;
    Rect rect = Rect(x, y, w, h) & bounds;
    //cout << "Drawing a rectangle at " << rect << endl;
    // change to circles
    rectangle(image, rect, color, 1);
  }
}

void ParticleFilter::redistribute(const float lbound[], const float ubound[]) {
  for (unsigned int i = 0; i < num_particles; i++) {
    for (unsigned int j = 0; j < num_states; j++) {
      float r = rng.uniform(lbound[j], ubound[j]);
      particles[i](j) = r;
      confidence[i] = 1.0f/(float)num_particles;
    }
  }
}

