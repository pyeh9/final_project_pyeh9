#include <opencv2/opencv.hpp>

#ifdef __cplusplus
extern "C" {

#endif
float calc_likelihood_gpu_kernel(cv::Mat& image_roi, cv::Mat& target_bhist, cv::Mat& target_ghist, cv::Mat& target_rhist);

//void calc_confidence_gpu(cv::Mat& image, const cv::Size& target_size, cv::Mat& target_bhist, cv::Mat& target_ghist, cv::Mat& target_rhist);

#ifdef __cplusplus
}
#endif
