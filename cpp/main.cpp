#include <iostream>
#include <unistd.h>
#include <string>
#include <cstdlib>
#include "state.h"
#include "filter.h"
#include "selector.h"
#include "hist.h"
#include <opencv2/opencv.hpp>
#include <fstream>
#include <opencv2/gpu/gpu.hpp>
#include <chrono>
#define NUM_PARTICLES 200
const char *WINDOW = "Particle Tracker";
const Scalar RED = Scalar(0, 0, 255);
const Scalar BLUE = Scalar(255, 0, 0);
const Scalar GREEN = Scalar(0, 255, 0);
const Scalar CYAN = Scalar(255, 255, 0);
const Scalar MAGENTA = Scalar(255, 0, 255);
const Scalar YELLOW = Scalar(0, 255, 255);
const Scalar WHITE = Scalar(255, 255, 255);
const Scalar BLACK = Scalar(0, 0, 0);

using namespace std;
using namespace cv;

struct Opts {
  int num_particles;
  string infile;
  string outfile;
  Opts() : num_particles(NUM_PARTICLES), infile(), outfile() {}
};
void parse(int argc, char **argv, Opts& o);

int main(int argc, char **argv) {
  Opts o;
  parse(argc, argv, o);
  VideoCapture cap;
  VideoWriter writer;
  
  cap.open(o.infile);
  // may need to supply fps manually
  int fps = cap.get(CV_CAP_PROP_FPS);
  //if (fps == -2147483648) {
  //  cerr << "FPS falsely detected. Exiting\n";
  //  exit(1);
  //}

  int fcount = cap.get(CV_CAP_PROP_FRAME_COUNT);
  int width = cap.get(CV_CAP_PROP_FRAME_WIDTH);
  int height = cap.get(CV_CAP_PROP_FRAME_HEIGHT);
  cout << fps << " " << fcount << " " << width << " " << height << endl;
  
  
  //if (!o.outfile.empty()) {
  //  writer.open(o.outfile, CV_FOURCC('P','I','M','1'), fps, Size(width, height));
  //  if (!writer.isOpened()) {
  //    cerr << "Could not open '" << o.outfile << "'" << endl;
  //    exit(1);
  //  }
  //}
  

  namedWindow(WINDOW, WINDOW_AUTOSIZE);
  // init process 
  // select patch
  StateData d(o.num_particles);
  cap >> d.image;
  int curFrame = 1;
  imshow(WINDOW, d.image);
  waitKey(10);
  while (!d.selector.valid()) {
    Mat roi(d.image, d.selector.selection());
    bitwise_not(roi, roi);
    imshow(WINDOW, d.image); // shows selected region in negative
    waitKey(10);
    bitwise_not(roi, roi);
  }
  d.selection = d.selector.selection();
  cout << "Target acquired at (" << d.selection << endl << ")\n";
  
  // timing code
  auto start = chrono::steady_clock::now();
    
  // generate initial histogram
  Mat roi(d.image, d.selection);
  roi.copyTo(d.target);
#ifdef USE_GPU
  // target histograms can entirely reside on GPU
  gpu::GpuMat target_hist_gpu[3];
  gpu::GpuMat target_gpu[3];
  Mat target_channel[3];
  split(roi, target_channel);
  target_gpu[0].upload(target_channel[0]);
  target_gpu[1].upload(target_channel[1]);
  target_gpu[2].upload(target_channel[1]);
  gpu::calcHist(target_gpu[0], target_hist_gpu[0]);
  gpu::calcHist(target_gpu[1], target_hist_gpu[1]);
  gpu::calcHist(target_gpu[2], target_hist_gpu[2]);
  //gpu::normalize(target_hist_gpu[0], target_hist_gpu[0], 1, 0, NORM_L2, CV_32F);
  //gpu::normalize(target_hist_gpu[1], target_hist_gpu[1], 1, 0, NORM_L2, CV_32F);
  //gpu::normalize(target_hist_gpu[2], target_hist_gpu[2], 1, 0, NORM_L2, CV_32F);
  target_hist_gpu[0].download(d.target_histogram[0]);
  target_hist_gpu[1].download(d.target_histogram[1]);
  target_hist_gpu[2].download(d.target_histogram[2]);
  normalize(d.target_histogram[0], d.target_histogram[0], 1, 0, NORM_L2, CV_32F);
  normalize(d.target_histogram[1], d.target_histogram[1], 1, 0, NORM_L2, CV_32F);
  normalize(d.target_histogram[2], d.target_histogram[2], 1, 0, NORM_L2, CV_32F);
  target_hist_gpu[0].release();
  target_hist_gpu[1].release();
  target_hist_gpu[2].release();  
  target_gpu[0].release();
  target_gpu[1].release();
  target_gpu[2].release();
  //cout << d.target_histogram[0] << endl;
  //cout << d.target_histogram[1] << endl;
  //cout << d.target_histogram[2] << endl;
#else
  calc_hist(roi, d.target_histogram[0], d.target_histogram[1], d.target_histogram[2]);
#endif
  cout << "Target histogram created\n";

  // initialize filter with center of selection
  d.filter.init(d.selection);
  Size target_size(d.target.cols, d.target.rows);
  cout << "Initial samples created\n";
  //d.filter.draw_particles(d.image, target_size, MAGENTA);
  //cout << "Drawing initial particles\n";
  imshow(WINDOW, d.image);
  waitKey(10);

  auto end = chrono::steady_clock::now();
  auto diff = end - start;
  auto per_frame = diff/(float)curFrame;
  cout << "Frame " << curFrame << ", elapsed: " 
       << chrono::duration<double, milli>(diff).count() << "ms ,";
  cout << "spf: "
       << chrono::duration<double, milli>(per_frame).count() 
       << "ms, ";
  cout << "fps: " 
       << (int)(1.0/chrono::duration<double>(per_frame).count()) 
       << endl; 

  // loop through the video, applying the filter and tracking
  cout << "Begin tracking...\n";
  while (curFrame != fcount) {
    cap >> d.image;
    curFrame++;
    
    // update
#ifdef USE_GPU
    d.filter.update_gpu(d.image, d.selection.size(), d.target_histogram[0], d.target_histogram[1], d.target_histogram[2]);
#else
    d.filter.update(d.image, d.selection.size(), d.target_histogram[0], d.target_histogram[1], d.target_histogram[2]);
#endif
    // draw particles
    //d.filter.draw_particles(d.image, target_size, MAGENTA);

    // draw estimated state
    float confidence = d.filter.get_confidence();
    if (confidence > 0.1) {
      d.filter.draw_estimated_state(d.image, target_size, GREEN);
    } else if (confidence > 0.025) {
      d.filter.draw_estimated_state(d.image, target_size, YELLOW);
    } else { 
      d.filter.draw_estimated_state(d.image, target_size, RED);
    }

    Rect target(d.image.cols-d.selection.width, 0, 
                d.selection.width, d.selection.height);
    Mat target_display_area(d.image, target);
    d.target.copyTo(target_display_area);
    imshow(WINDOW, d.image);
    
    // timing code
    end = chrono::steady_clock::now();
    diff = end - start;
    per_frame = diff/(float)curFrame;
    cout << "Frame " << curFrame << ", Elapsed time: " 
         << chrono::duration<double, milli>(diff).count() << "ms, ";
    cout << "Time per frame: "
         << chrono::duration<double, milli>(per_frame).count() 
         << "ms, ";
    cout << "fps: " 
         << (int)(1.0/chrono::duration<double>(per_frame).count()) 
         << endl; 
    waitKey(10);
  }
  return 0;
}

/*********************************************************
 *
 * Extra functions
 *
 *********************************************************/
void parse(int argc, char **argv, Opts& o) {
  int c = -1;
  o.outfile = "";
  while( (c = getopt(argc, argv, "o:p:")) != -1 ) {
    switch(c) {
    //case 'o': o.outfile = optarg; break;
    case 'p': o.num_particles = atoi(optarg); break;
    default:
      cerr << "Usage: " << argv[0] << " [-o output_file] [-p num_particles] [input_file]\n\n";
      cerr << "\t-o output_file : Optional mjpeg output file\n";
      cerr << "\t-p num_particles: Number of particles (samples) to use, default is 200\n";
      cerr << "\tinput_file : file to read\n";
	    exit(1);
    }
   }
   if(optind < argc) o.infile = argv[optind];
   else { cerr << "Error: no input file detected\n"; exit(1); }
   cout << "Num particles: " << o.num_particles << endl;
   cout << "Input file: " << o.infile << endl;
   cout << "Output file: " << o.outfile << endl;
}

