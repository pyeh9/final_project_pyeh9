#include "filter_cuda.h"
#include <vector>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <opencv2/gpu/devmem2d.hpp>

/*
float ParticleFilter::calc_likelihood(Mat& image_roi, Mat& target_bhist, Mat& target_ghist, Mat& target_rhist) {
  static const float LAMBDA = 20.0f;
  static Mat ghist, bhist, rhist;
  calc_hist(image_roi, ghist, bhist, rhist);
  
  float b = compareHist(target_bhist, bhist, CV_COMP_BHATTACHARYYA);
  float g = compareHist(target_ghist, ghist, CV_COMP_BHATTACHARYYA);
  float r = compareHist(target_rhist, rhist, CV_COMP_BHATTACHARYYA);
  float similar = 0.2126*r + 0.7152*g + 0.0722*b;
  // Y = 0.2126R+0.7152G+0.0722B
  float prob = exp(-LAMBDA*(similar*similar));
  return prob;
}
*/

// calculates likelihood for a (ROI, target) pair
__global__ void 
calc_likelihood_gpu_kernel() {
  
}



