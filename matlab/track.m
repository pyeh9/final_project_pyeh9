%% define patch & initialize everything
facecr = 118; 
facecc = 187;
wid = 55;
ht = 81;
t = facecr-floor(ht/2);
b = facecr+floor(ht/2);
l = facecc-floor(wid/2);
r = facecc+floor(wid/2);
face = mov(1).cdata(t:b, l:r, :);
[M N x] = size(mov(1).cdata);
% initial
numSamples = 50; % initially, 250, optimally with ~50
mov(1).r = facecr*ones(1,numSamples);
mov(1).c = facecc*ones(1,numSamples);
sigmasq = 10000;

% calculate weights
mov(1).p = 0;
for k = 1:length(mov(1).r)
    mov(1).p(k) = exp(-myMSE(mov(1).cdata, face, mov(1).r(k), mov(1).c(k))/(2*sigmasq));
end
% normalize weights
mov(1).p = (mov(1).p)/sum(mov(1).p);

mov(1).trackc = sum(mov(1).p .* mov(1).c)/sum(mov(1).p);
mov(1).trackr = sum(mov(1).p .* mov(1).r)/sum(mov(1).p);
% imshow(mov(1).cdata); hold on;
% plotFrame(mov(1).trackr, mov(1).trackc, wid, ht);
% hold off

%% iterations
tic;
dynamic = 4;
%distribution = zeros(M,N);
distribution = ones(M,N)/(M*N);

for i = 2:numFrames
    i
    % update create new distribution based on weights
    distribution = updateDist(distribution, mov(i-1).r, mov(i-1).c, mov(i-1).p);
    %imshow(distribution, [min(distribution(:)) max(distribution(:))]);
    % resample
    samp = randsample(M*N, numSamples, true, reshape(distribution',1,M*N))';
    mov(i).r = ceil(samp/N);
    mov(i).c = mod(samp,N);
    mov(i).p = 0;   
    % calculate weights
    for k = 1:length(mov(i).r)
        mov(i).r(k) = mov(i).r(k) + round(dynamic*randn());
        mov(i).c(k) = mov(i).c(k) + round(dynamic*randn());
        mov(i).p(k) = exp(-myMSE(mov(i).cdata, face, mov(i).r(k), mov(i).c(k))/(2*sigmasq));
    end
    % remove really low weights and normalize 
    mov(i).p(mov(i).p < prctile(mov(i).p, 15)) = 0;
    mov(i).p = (mov(i).p)/sum(mov(i).p);
    % weighted mean of samples gives predicted 
    mov(i).trackc = sum(mov(i).p .* mov(i).c)/sum(mov(i).p);
    mov(i).trackr = sum(mov(i).p .* mov(i).r)/sum(mov(i).p);
    
    % uncomment for visuals
    imshow(mov(i).cdata); hold on;
    plotFrame(mov(i).trackr, mov(i).trackc, wid, ht);
    scatter(mov(i).c, mov(i).r, '.');
    plot(mov(i).trackc, mov(i).trackr, 'r+');
    spread = std(sqrt((mov(i).r-mov(i).trackr).^2+(mov(i).c-mov(i).trackc).^2));
    [x y] = plot_ellipse(mov(i).trackc, mov(i).trackr, 2*spread, 2*spread, 0); % scaled spread for visibility
    plot(x,y, 'g');
    hold off;
    pause(0.0001);
end
toc;







