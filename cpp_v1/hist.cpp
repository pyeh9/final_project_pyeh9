#include "hist.h"

using namespace std;
using namespace cv;

void calc_hist(Mat& bgr, Mat& bhist, Mat& ghist, Mat& rhist) {
  //static const int channels[] = {0, 1, 2};
  static const int b_bins = 8;
  static const int g_bins = 8;
  static const int r_bins = 8;
  static const int hist_size[] = {b_bins, g_bins, r_bins};
  static const float branges[] = {0, 256};
  static const float granges[] = {0, 256};
  static const float rranges[] = {0, 256};
  static const float *ranges[] = {rranges, granges, branges};
  //static const Mat mask;
  Mat channel[3];
  split(bgr, channel);

  cv::calcHist(&channel[0], 1, 0, Mat(), bhist, 1, 
           &hist_size[0], &ranges[0], true, false);
  cv::calcHist(&channel[1], 1, 0, Mat(), ghist, 1, 
           &hist_size[1], &ranges[1], true, false);
  cv::calcHist(&channel[2], 1, 0, Mat(), rhist, 1, 
           &hist_size[2], &ranges[2], true, false);
  cv::normalize(bhist, bhist, 1, 0, NORM_L2, CV_32F);
  cv::normalize(ghist, ghist, 1, 0, NORM_L2, CV_32F);
  cv::normalize(rhist, rhist, 1, 0, NORM_L2, CV_32F);
}

