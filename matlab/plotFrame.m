function plotFrame(r, c, wid, ht)
    
plot([c-floor(wid/2) c+floor(wid/2) c+floor(wid/2) c-floor(wid/2) c-floor(wid/2)],...   
     [r-floor(ht/2)  r-floor(ht/2)  r+floor(ht/2)  r+floor(ht/2)  r-floor(ht/2)], 'r-');
 
end
