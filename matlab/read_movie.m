% read movie
clear 
clc
tic;
obj = mmreader('../input_images/pres_debate.avi');
vidFrames = read(obj);
%numFrames = get(obj, 'numberOfFrames');
numFrames = 150;
mov(numFrames).cdata = 0;
for k = 1:numFrames
    k
    mov(k).cdata = imresize(vidFrames(:,:,:,k),0.5);
    %mov(k).cdata = mat2gray(rgb2gray(mov(k).cdata));
    mov(k).cdata = mat2gray(mov(k).cdata);
end
toc;

